package com.runemate.ui.control.setting;

import com.runemate.ui.converter.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import com.google.common.base.*;
import java.util.Objects;
import java.util.stream.*;
import javafx.scene.control.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@Accessors(fluent = true)
public class SettingIntSpinner extends Spinner<Integer> implements SettingControl {

    private final SettingsManager manager;
    private final SettingsDescriptor group;
    private final SettingDescriptor setting;

    public SettingIntSpinner(final SettingsManager manager, final SettingsDescriptor group, final SettingDescriptor setting) {
        this.manager = manager;
        this.group = group;
        this.setting = setting;

        managedProperty().bind(visibleProperty());
        setDisable(setting.setting().disabled());
        setFocusTraversable(true);
        disableProperty().bind(manager.lockedProperty());
        setEditable(true);

        final var range = setting.range();
        final var initialStr = manager.get(group, setting);
        final var initial = Strings.isNullOrEmpty(initialStr) ? 0 : Integer.parseInt(initialStr);
        final var factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE);

        if (setting.suffix() != null) {
            factory.setConverter(new SuffixConverter(setting.suffix()));
        }

        valueProperty().addListener((obs, old, value) -> manager.set(group, setting, String.valueOf(value)));

        if (range != null) {
            factory.setMin(range.min());
            factory.setMax(range.max());
            factory.setAmountToStepBy(range.step());

            //Apply constraint in case previous value is outside of updated range
            factory.setValue(Math.min(Math.max(initial, range.min()), range.max()));
        } else {
            factory.setValue(initial);
        }

        setValueFactory(factory);

        if (!isDependencyMet()) {
            setVisible(false);
        }
    }


}
