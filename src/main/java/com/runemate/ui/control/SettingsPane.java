package com.runemate.ui.control;

import com.runemate.ui.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import java.util.*;

import javafx.beans.binding.Bindings;
import javafx.geometry.*;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;

public class SettingsPane extends TitledPane {

    private final SettingsManager manager;
    private final FlowPane container;
    private final DefaultUI parent;

    public SettingsPane(@NonNull DefaultUI ui, @NonNull SettingsManager manager) {
        this.manager = manager;
        this.parent = ui;

        final var parent = new VBox();
        parent.setSpacing(12);
        container = new FlowPane();
        container.setHgap(12);
        container.setVgap(12);
        container.setRowValignment(VPos.TOP);
        parent.setPadding(new Insets(12, 4, 4, 4));
        parent.getChildren().add(container);

        final var startButton = new Button("Start");
        startButton.managedProperty().bind(visibleProperty());
        startButton.visibleProperty().bind(manager.lockedProperty().not());
        startButton.setPrefHeight(24);
        startButton.setOnAction(e -> {
            manager.lock();
            startButton.setText("Update");
        });
        parent.getChildren().add(startButton);

        setText("Settings");
        setContent(parent);
        addSections();
        final var firstVisible = (SettingsSectionPane) container.getChildren().stream().filter(Node::isVisible).findFirst().orElse(null);
        if (firstVisible != null) {
            startButton.prefWidthProperty().bind(firstVisible.widthProperty());
        } else {
            startButton.setPrefWidth(250);
        }
    }

    private void addSections() {
        final var settings = new ArrayList<>(manager.getSettingsDescriptors());
        Collections.reverse(settings);
        for (final var group : settings) {
            for (final var section : group.sections()) {
                final var pane = new SettingsSectionPane(parent, section, section.section().collapsed());
                pane.init(manager, group);
                container.getChildren().add(pane);
            }

            //Add the top-level group
            final var pane = new SettingsSectionPane(parent);
            pane.init(manager, group);
            container.getChildren().add(0, pane);
        }
    }
}
