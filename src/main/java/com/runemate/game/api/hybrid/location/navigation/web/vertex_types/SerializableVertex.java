package com.runemate.game.api.hybrid.location.navigation.web.vertex_types;

import java.io.*;

public interface SerializableVertex {
    int getOpcode();

    boolean serialize(ObjectOutput stream);

    boolean deserialize(int protocol, ObjectInput stream);
}
