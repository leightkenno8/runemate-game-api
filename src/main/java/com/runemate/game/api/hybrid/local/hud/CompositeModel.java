package com.runemate.game.api.hybrid.local.hud;

import com.google.common.hash.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * A Model composed on other models.
 */
public class CompositeModel extends Model {
    private final List<Model> models;
    private BoundingModel cached_bounding_model;

    public CompositeModel(final Model... models) {
        this(Arrays.asList(models));
    }

    public CompositeModel(final List<Model> models) {
        super(null);
        this.models = models;
    }

    @Override
    public int hashCode() {
        final Hasher hasher = Hashing.md5().newHasher();
        models.forEach(model -> hasher.putInt(model.hashCode()));
        return hasher.hash().asInt();
    }

    @Override
    public Set<Color> getDefaultColors() {
        final Set<Color> colors = new HashSet<>();
        models.forEach(model -> colors.addAll(model.getDefaultColors()));
        return colors;
    }

    @Override
    public int getHeight() {
        //maxHeight - minHeight
        return 0;
    }

    @Override
    public List<Triangle> projectTriangles() {
        return projectTrianglesWithin(Projection.getViewport());
    }

    @Override
    public List<Triangle> projectTrianglesWithin(Shape viewport) {
        final List<Triangle> triangles = new ArrayList<>(25);
        models.forEach(model -> triangles.addAll(model.projectTrianglesWithin(viewport)));
        return triangles;
    }

    @Override
    protected int getTriangleCount() {
        int count = 0;
        for (Model model : models) {
            count += model.getTriangleCount();
        }
        return count;
    }

    @Override
    public boolean isValid() {
        for (final Model model : models) {
            if (model.isValid()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public BoundingModel getBoundingModel() {
        if (cached_bounding_model != null) {
            return cached_bounding_model;
        }
        int[] left = { Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE },
            right = { Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE };
        for (final Model model : models) {
            Pair<int[], int[]> source = model.getBoundingModel().getSourceValues();
            if (source.getLeft()[0] < left[0]) {
                left[0] = source.getLeft()[0];
            }
            if (source.getLeft()[1] < left[1]) {
                left[1] = source.getLeft()[1];
            }
            if (source.getLeft()[2] < left[2]) {
                left[2] = source.getLeft()[2];
            }
            if (source.getRight()[0] > right[0]) {
                right[0] = source.getRight()[0];
            }
            if (source.getRight()[1] > right[1]) {
                right[1] = source.getRight()[1];
            }
            if (source.getRight()[2] > right[2]) {
                right[2] = source.getRight()[2];
            }
        }
        return cached_bounding_model = new BoundingModel(owner, new Pair<>(left, right));
    }

    @Override
    public String toString() {
        return "CompositeModel{components=" + models.size() + '}';
    }

    @Override
    public boolean equals(Object o) {
        return this == o || super.equals(o) && models.equals(((CompositeModel) o).models);
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }
}
