package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class IdentityKitLoader extends SerializedFileLoader<CacheIdentityKit> {
    public IdentityKitLoader(int archive, boolean rs3) {
        super(archive);
    }

    @Override
    protected CacheIdentityKit construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheIdentityKit(file, false);
    }
}
