package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import javax.annotation.*;

/**
 * A non-player character
 */
public interface Npc extends Actor, Identifiable {
    /**
     * Gets the npcs current level
     *
     * @return the npcs level, otherwise -1
     */
    int getLevel();

    /**
     * A definition containing a vast collection of data regarding this npcs creation on the world-graph
     *
     * @return The definition, otherwise null
     */
    @Nullable
    NpcDefinition getDefinition();

    /**
     * Returns the active 'local state' of the NpcDefinition if one is present, otherwise returns the base NpcDefinition.
     */
    @Nullable
    default NpcDefinition getActiveDefinition() {
        final NpcDefinition base = getDefinition();
        final NpcDefinition local;
        return base != null && (local = base.getLocalState()) != null ? local : base;
    }
}
