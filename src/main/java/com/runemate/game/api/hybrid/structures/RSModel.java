package com.runemate.game.api.hybrid.structures;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;

public abstract class RSModel extends Model {
    protected final long uid;

    public RSModel(long uid, final LocatableEntity entity, final int extraFloorHeight) {
        super(entity, extraFloorHeight);
        this.uid = uid;
    }


    @Override
    public boolean isValid() {
        return OpenClient.validate(uid);
    }

    @Override
    public String toString() {
        return "Model(origin: game)";
    }
}
