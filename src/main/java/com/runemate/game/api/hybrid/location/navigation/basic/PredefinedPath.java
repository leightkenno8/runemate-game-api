package com.runemate.game.api.hybrid.location.navigation.basic;

import com.runemate.game.api.hybrid.location.*;
import java.util.*;
import javax.annotation.*;

/**
 * A predefined list of coordinates to take steps on (the direct equivalent to the "TilePath" class of some other APIs)
 */
public final class PredefinedPath extends CoordinatePath {
    private final List<Coordinate> path;

    private PredefinedPath(final Coordinate... path) {
        this(Arrays.asList(path));
    }

    private PredefinedPath(final List<Coordinate> path) {
        this.path = Collections.unmodifiableList(path);
    }

    public static PredefinedPath create(@Nonnull final Coordinate... coordinates) {
        return new PredefinedPath(coordinates);
    }

    public static PredefinedPath create(@Nonnull final List<Coordinate> coordinates) {
        return new PredefinedPath(coordinates);
    }

    @Override
    public List<Coordinate> getVertices() {
        return path;
    }
}
