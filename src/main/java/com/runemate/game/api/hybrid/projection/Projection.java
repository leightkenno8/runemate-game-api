package com.runemate.game.api.hybrid.projection;

import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.osrs.projection.*;
import java.awt.*;
import java.util.*;

public final class Projection {

    private Projection() {
    }

    /**
     * Calculates the position of the specified coordinate coordinate on the mininmap
     */
    public static InteractablePoint coordinateToMinimap(final Coordinate coordinate) {
        return OSRSProjection.coordinateToMinimap(coordinate);
    }

    /**
     * Projects an absolute point in 3d space to it's screen point.
     */
    public static Point[] multiAbsoluteToScreen(
        int height, final int[] xs, final int[] ys, final int[] zs, final Map<String, Object> cache
    ) {
        return OSRSProjection.multiAbsoluteToScreen(height, xs, ys, zs, cache);
    }

    public static Shape getViewport() {
        return OSRSProjection.getViewport();
    }
}
