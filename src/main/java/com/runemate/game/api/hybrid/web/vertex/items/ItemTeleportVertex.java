package com.runemate.game.api.hybrid.web.vertex.items;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.hybrid.web.vertex.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@ToString(callSuper = true)
public abstract class ItemTeleportVertex extends TeleportVertex {

    protected final Pattern action;
    protected final @ToString.Exclude SpriteItemQueryBuilder builder;

    public ItemTeleportVertex(final Coordinate position, final Pattern action, final SpriteItemQueryBuilder builder) {
        super(position);
        this.action = action;
        this.builder = builder;
    }

    @Override
    public ScanResult scan(final Map<String, Object> cache) {
        if (position.isVisible()) {
            return new ScanResult(null, ScanAction.CONTINUE);
        }

        final var item = getItem();
        return new ScanResult(item != null ? this : null, ScanAction.CONTINUE);
    }

    @Nullable
    public SpriteItem getItem() {
        return builder.results().first();
    }
}
