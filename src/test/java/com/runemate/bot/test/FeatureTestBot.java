package com.runemate.bot.test;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.setting.annotation.open.*;
import lombok.extern.log4j.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 * <p>
 * The following gradle command will build this bot and then launch the client:
 * testJar launch --args="--dev"
 */
@Log4j2
public class FeatureTestBot extends LoopingBot implements RegionListener {

    @SettingsProvider(updatable = true)
    private ExampleSettings settings;

    @Override
    public void onStart(final String... arguments) {
        getEventDispatcher().addListener(this);
        setLoopDelay(600);
    }

    @Override
    public void onLoop() {
        for (var item : Inventory.getItems()) {
            var ge = GrandExchange.lookup(item.getId());
            if (ge != null) {
                log.info("Item with id {} has price {}", item.getId(), ge.getPrice());
            } else {
                log.warn("Failed to resolve item {} on GE", item);
            }
        }
    }

    @Override
    public void onRegionLoaded(final RegionLoadedEvent event) {
        log.info(event);
    }
}
